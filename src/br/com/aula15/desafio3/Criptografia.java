/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.aula15.desafio3;

/**
 *
 * @author diegomoraes
 */
public class Criptografia {

    public String cripto(int parte1, int parte2) {

        String cripto1 = "";

        for (int i = 0; i < parte1; i++) {

            for (int j = 0; j < parte2; j++) {

                if (i % 2 == 0 && j % 2 == 0) {
                    
                    cripto1 = cripto1+"X";

                }else{
                    cripto1 = cripto1+"Y";
                }

            }

        }
        
        return cripto1;

    }

}
