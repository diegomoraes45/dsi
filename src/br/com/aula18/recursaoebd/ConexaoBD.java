/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.aula18.recursaoebd;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author diegomoraes
 */
public class ConexaoBD {
    
    //Método para Conectar
     public Connection conector(JFrame exemploConexao) {
         //Declaração da variável para conexão
         Connection conexao = null;
         
         //Declaração da variável para receber o Driver de conexão do banco
        String driver = "com.mysql.jdbc.Driver";

        try {
            Class.forName(driver); //Carregar o driver do Banco de Dados
            conexao = DriverManager.getConnection("jdbc:mysql://localhost/svl", "root", ""); //Atribuir parâmetros de conexão à variável conexao
            JOptionPane.showMessageDialog(exemploConexao, "Conectado com Sucesso");
            return conexao;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(exemploConexao, "Erro ao Conectar!");
            return null;
        }

    }
     
    //Método para Desconectar
    public void desconector(Connection con, JFrame exemploConexao) {
        try {
            con.close();
            JOptionPane.showMessageDialog(exemploConexao, "Desconectado com Sucesso");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(exemploConexao, "Erro ao desconectar "+e);
        }
    }

    
}
